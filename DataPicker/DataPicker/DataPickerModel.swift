//
//  DataPickerModel.swift
//  DataPicker
//
//  Created by Kuo-Chuan Pan on 7/11/15.
//  Copyright © 2015 Kuo-Chuan Pan. All rights reserved.
//

import Foundation

enum PlotType {
    case data, ruler
}

class DataPickerModel {
    var graphMode:Int
    
    // dataPlot
    var vxmin:CGPoint  // xmin in view coordinates
    var vxmax:CGPoint
    var vymin:CGPoint
    var vymax:CGPoint
    
    var px1:CGFloat
    var px2:CGFloat
    var py1:CGFloat
    var py2:CGFloat
    
    var xlog:Bool
    var ylog:Bool
    
    init() {
        graphMode = 1
        vxmin = CGPointMake(0,0)
        vxmax = CGPointMake(0,0)
        vymin = CGPointMake(0,0)
        vymax = CGPointMake(0,0)
        xlog = false
        ylog = false
        px1 = 0.0
        px2 = 0.0
        py1 = 0.0
        py2 = 0.0
    }
    
    func setMode(mode:Int) {
        self.graphMode = mode
    }
    
    func dataPlot_setControlPoints (vxmin: CGPoint, vxmax: CGPoint, vymin: CGPoint, vymax: CGPoint){
        self.vxmin = vxmin
        self.vxmax = vxmax
        self.vymin = vymin
        self.vymax = vymax
    }
    func dataPlot_setLogs(isXlog:Bool, isYlog:Bool){
        self.xlog = isXlog
        self.ylog = isYlog
    }
    
    func dataPLot_setPhysicalPoints(px1:CGFloat, px2:CGFloat, py1:CGFloat, py2:CGFloat){
        if self.xlog {
            self.px1 = log10(px1)
            self.px2 = log10(px2)
            
        } else {
            self.px1 = px1
            self.px2 = px2
            
        }
        if self.ylog {
            self.py1 = log10(py1)
            self.py2 = log10(py2)
        } else {
            self.py1 = py1
            self.py2 = py2
        }
    }
    
    
    func dataPlot_getPhysicalPoints(vpt:CGPoint) -> CGPoint {
        
        let vx1 :CGFloat = vxmin.x
        let vx2 :CGFloat = vxmax.x
        let vy1 :CGFloat = vymin.y
        let vy2 :CGFloat = vymax.y
        let vx:CGFloat = vpt.x
        let vy:CGFloat = vpt.y
        var px:CGFloat = (vx - vx1) * ((px2 - px1) / (vx2 - vx1)) + px1
        var py:CGFloat = (vy - vy1) * ((py2 - py1) / (vy2 - vy1)) + py1
        
        if xlog {
            px = pow(10,px)
        }
        if ylog {
            py = pow(10,py)
        }
        return CGPointMake(px, py)
    }
    
    
}