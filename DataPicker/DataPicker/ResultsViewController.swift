//
//  ResultsViewController.swift
//  DataPicker
//
//  Created by Kuo-Chuan Pan on 7/11/15.
//  Copyright © 2015 Kuo-Chuan Pan. All rights reserved.
//

import UIKit

class ResultsViewController: UIViewController, UINavigationControllerDelegate, UITextViewDelegate {

    @IBOutlet weak var resultTextView: UITextView!
    var resultString:String = ""
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
        self.navigationController?.navigationBar.hidden = false
        resultTextView.delegate = self
        resultTextView.text = resultString
        
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
}
