
//
//  AimView.swift
//  DataPicker
//
//  Created by Kuo-Chuan Pan on 7/5/15.
//  Copyright © 2015 Kuo-Chuan Pan. All rights reserved.
//

import Foundation
class AimView: UIView {
    var aimColor = UIColor.redColor()
    var touchPoint = CGPointMake(0,300)
    
    required init?(coder aDecoder: NSCoder){
        super.init(coder: aDecoder)
    }
    
    override init (frame: CGRect) {
        super.init(frame: frame)
        backgroundColor = UIColor.clearColor()
    }
    
    override func drawRect(rect: CGRect) {
        let label:UIBezierPath = UIBezierPath()
        aimColor.set()
        
        label.moveToPoint(CGPointMake(40,50))
        label.addLineToPoint(CGPointMake(60, 50))
        label.lineWidth = 1
        label.stroke()
        
        label.moveToPoint(CGPointMake(50,40))
        label.addLineToPoint(CGPointMake(50, 60))
        label.lineWidth = 1
        label.stroke()
    }
    
    func setMyTouchPoint (pt: CGPoint) {
        self.touchPoint = pt
        self.center = CGPointMake(pt.x, pt.y)
        
    }
}