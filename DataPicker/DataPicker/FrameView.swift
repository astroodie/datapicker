//
//  FrameView.swift
//  DataPicker
//
//     USE:  framView contains four corners   
//
//  Created by Kuo-Chuan Pan on 7/4/15.
//  Copyright © 2015 Kuo-Chuan Pan. All rights reserved.
//

import UIKit

protocol FrameViewDataSource: class {
    func ulPtForFrameView(sender: FrameView) -> CGPoint?
    func urPtForFrameView(sender: FrameView) -> CGPoint?
    func llPtForFrameView(sender: FrameView) -> CGPoint?
    func lrPtForFrameView(sender: FrameView) -> CGPoint?
}

class FrameView: UIView {

    var ulPt: CGPoint!{
        didSet{
            self.setNeedsDisplay()
        }
    }
    var urPt: CGPoint!{
        didSet{
            self.setNeedsDisplay()
        }
    }
    var llPt: CGPoint!{
        didSet{
            self.setNeedsDisplay()
        }
    }
    var lrPt: CGPoint!{
        didSet{
            self.setNeedsDisplay()
        }
    }
    
    weak var dataSource: FrameViewDataSource?
    
    // Only override drawRect: if you perform custom drawing.
    // An empty implementation adversely affects performance during animation.
    
    
    override func drawRect(rect: CGRect) {
        // Drawing code
        
        // set the corners
        
        ulPt = dataSource?.ulPtForFrameView(self) ?? CGPointMake(50, 50)
        urPt = dataSource?.urPtForFrameView(self) ?? CGPointMake(250, 50)
        llPt = dataSource?.llPtForFrameView(self) ?? CGPointMake(50, 250)
        lrPt = dataSource?.lrPtForFrameView(self) ?? CGPointMake(250, 250)
        
        // draw boundary
        
        let marker: UIBezierPath = UIBezierPath()
        let lineColor = UIColor.redColor()
        lineColor.set()
        marker.lineWidth = 2
        marker.moveToPoint(ulPt)
        marker.addLineToPoint(urPt)
        marker.stroke()
        marker.moveToPoint(urPt)
        marker.addLineToPoint(lrPt)
        marker.stroke()
        marker.moveToPoint(lrPt)
        marker.addLineToPoint(llPt)
        marker.stroke()
        marker.moveToPoint(llPt)
        marker.addLineToPoint(ulPt)
        marker.stroke()
        
        
    }
    
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        
        //let panRecognizer = UIPanGestureRecognizer(target:self, action:"detectPan:")
        //self.gestureRecognizers = [panRecognizer]
        self.backgroundColor = UIColor.clearColor()
        
    }
    
    required init(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    //func detectPan(recognizer:UIPanGestureRecognizer) {
        //let translation  = recognizer.translationInView(self.superview!)
        //self.center = CGPointMake(lastLocation.x + translation.x, lastLocation.y + translation.y)
        //print("Frame pan")
    //}

    override func touchesBegan(touches: Set<UITouch>, withEvent event: UIEvent?) {
        //self.superview?.bringSubviewToFront(self)
        //print("Frame touches")
    }

}
