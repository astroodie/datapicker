//
//  EditViewController.swift
//  DataPicker
//
//  Created by Kuo-Chuan Pan on 7/3/15.
//  Copyright © 2015 Kuo-Chuan Pan. All rights reserved.
//

import UIKit

class EditViewController: UIViewController, UINavigationControllerDelegate, FrameViewDataSource, CornerViewDelegate {

    @IBOutlet weak var selectedImageView: UIImageView!
    
    
    var frameView : FrameView!{
        didSet{
            frameView.dataSource = self
        }
    }
    var magnifier : MagnifierView!{
        didSet{
            magnifier.superview?.bringSubviewToFront(magnifier)
        }
    }
    var plotType:Int = 1
    var selectedImage : UIImage!
    var outImage: UIImage = UIImage()
    var hasFound: Bool = false
    
    var upperLeftCornerView : CornerView!
    var upperRightCornerView : CornerView!
    var lowerLeftCornerView : CornerView!
    var lowerRightCornerView : CornerView!
    var origin:CGPoint! = CGPointMake(0, 0)

    
    //
    // MARK: from CornerViewDelegate
    //
    
    func cornerFinished(center:CGPoint){
        magnifier.hidden = true
    }
    func cornerTapped(center:CGPoint){
        magnifier.hidden = false
        magnifier.setMyTouchPoint(center)
        magnifier.setNeedsDisplay()
    }
    func cornerChanged(center:CGPoint){
        // update Magnifier
        magnifier.superview?.bringSubviewToFront(magnifier)
        magnifier.setMyTouchPoint(center)
        magnifier.setNeedsDisplay()
    
        // update frame
        frameView.setNeedsDisplay()
        
    }
    
    //
    // MARK: from FrameViewDataSource
    //
    func ulPtForFrameView(sender: FrameView) -> CGPoint? {
        return upperLeftCornerView.center
    }
    func urPtForFrameView(sender: FrameView) -> CGPoint? {
        return upperRightCornerView.center
    }
    func llPtForFrameView(sender: FrameView) -> CGPoint? {
        return lowerLeftCornerView.center
    }
    func lrPtForFrameView(sender: FrameView) -> CGPoint? {
        return lowerRightCornerView.center
    }
   
    override func viewWillAppear(animated: Bool) {
        super.viewWillAppear(true)
        selectedImageView.setNeedsLayout()
        selectedImageView.layoutIfNeeded()
        self.navigationController?.navigationBar.hidden = false
    }
    override func viewDidAppear(animated: Bool) {
        
        if !hasFound {
            // draw bound frame
            frameView = FrameView(frame:CGRectMake(0, 0, selectedImageView.frame.width, selectedImageView.frame.height))
            frameView.dataSource = self
            
            // find bound box
            self.findBoundBox()
            
        }
        
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()

        
        // Do any additional setup after loading the view.
        
        // draw magnifier
        let center = CGPointMake(0, 0)
        magnifier = MagnifierView(frame: CGRectMake(center.x-100,center.y-100,100,100))
        magnifier.viewToMagnify = selectedImageView
        magnifier.hidden = true
        selectedImageView.addSubview(magnifier)

        
    }
    
    func findBoundBox(){
        
        let boundResults = DataPickerOpenCV.cvGetRect(selectedImage)
        let outputImage  = boundResults[0] as! UIImage // is the resized image
        let success      = (boundResults[1] as! NSNumber).boolValue
        
        //let boundRect = CGRectMake(CGFloat(boundResults[2] as! NSNumber), CGFloat(boundResults[3] as! NSNumber), CGFloat(boundResults[4] as! NSNumber), CGFloat(boundResults[5] as! NSNumber))
        selectedImage = outputImage
        selectedImageView.image = selectedImage
        
        let ptl:CGPoint = (boundResults[6] as! NSValue).CGPointValue()
        let ptr:CGPoint = (boundResults[7] as! NSValue).CGPointValue()
        let pbl:CGPoint = (boundResults[8] as! NSValue).CGPointValue()
        let pbr:CGPoint = (boundResults[9] as! NSValue).CGPointValue()
        
        let imageRatio:CGFloat = selectedImage.size.width / selectedImage.size.height
        let viewRatio:CGFloat = selectedImageView.frame.size.width / selectedImageView.frame.size.height
        let imgWidth:CGFloat = 1200 * imageRatio
        let imgHeight:CGFloat = 1200
        
        // the image size in imageview coordinate
        var outWidth:CGFloat  = 0
        var outHeight:CGFloat = 0
        
        if imageRatio >= viewRatio {
            // width > height
            outWidth  = selectedImageView.frame.size.width
            outHeight = outWidth / imageRatio
            origin = CGPointMake(0, (selectedImageView.frame.height - outHeight) / 2)
            
        } else {
            // height > width
            outHeight = selectedImageView.frame.size.height
            outWidth  = outHeight * imageRatio
            origin = CGPointMake((selectedImageView.frame.width - outWidth) / 2, 0)
        }
        
        
        if success {
            print("find the bound box! ")
            //let origin:CGPoint = CGPointMake(0, 0)
            let vtl:CGPoint = CGPointMake(origin.x + (ptl.x / imgWidth) * outWidth, origin.y + (ptl.y / imgHeight) * outHeight)
            let vtr:CGPoint = CGPointMake(origin.x + (ptr.x / imgWidth) * outWidth, origin.y + (ptr.y / imgHeight) * outHeight)
            let vbl:CGPoint = CGPointMake(origin.x + (pbl.x / imgWidth) * outWidth, origin.y + (pbl.y / imgHeight) * outHeight)
            let vbr:CGPoint = CGPointMake(origin.x + (pbr.x / imgWidth) * outWidth, origin.y + (pbr.y / imgHeight) * outHeight)
            
            let cornerSize:CGFloat = 20
            upperLeftCornerView = CornerView(frame:CGRectMake(vtl.x - 10 , vtl.y - 10, cornerSize, cornerSize))
            upperRightCornerView = CornerView(frame:CGRectMake(vtr.x - 10, vtr.y - 10, cornerSize, cornerSize))
            lowerLeftCornerView = CornerView(frame:CGRectMake(vbl.x - 10, vbl.y - 10, cornerSize, cornerSize))
            lowerRightCornerView = CornerView(frame:CGRectMake(vbr.x - 10, vbr.y - 10, cornerSize, cornerSize))
            frameView.setNeedsDisplay()
            
        } else {
            print("didn't find the bound box. use the default one")
            // default locations
            let cornerSize:CGFloat = 20
            
            // print("width \(width)") // = 414 for ip6+
            
            upperLeftCornerView = CornerView(frame:CGRectMake(0.2 * outWidth + origin.x - 10, 0.2 * outHeight + origin.y - 10, cornerSize, cornerSize))
            upperRightCornerView = CornerView(frame:CGRectMake(0.8 * outWidth + origin.x - 10, 0.2 * outHeight + origin.y - 10, cornerSize, cornerSize))
            lowerLeftCornerView = CornerView(frame:CGRectMake(0.2 * outWidth + origin.x - 10, 0.8 * outHeight + origin.y - 10, cornerSize, cornerSize))
            lowerRightCornerView = CornerView(frame:CGRectMake(0.8 * outWidth + origin.x - 10, 0.8 * outHeight + origin.y - 10, cornerSize, cornerSize))
        }
        
        // draw corners
        upperLeftCornerView.delegate = self
        upperRightCornerView.delegate = self
        lowerLeftCornerView.delegate = self
        lowerRightCornerView.delegate = self
        
        selectedImageView.addSubview(frameView)
        selectedImageView.addSubview(upperLeftCornerView)
        selectedImageView.addSubview(upperRightCornerView)
        selectedImageView.addSubview(lowerLeftCornerView)
        selectedImageView.addSubview(lowerRightCornerView)
        hasFound = true
        
        // adjust corner points if exceed boundary
        let buffer:CGFloat = 15
        
        for vw in self.selectedImageView.subviews {
            if vw is CornerView {
                if vw.center.x > self.selectedImageView.frame.width - buffer {
                    vw.center.x = self.selectedImageView.frame.width - buffer
                }
                if vw.center.x < buffer {
                    vw.center.x = buffer
                }
                if vw.center.y > self.selectedImageView.frame.height - buffer {
                    vw.center.y = self.selectedImageView.frame.height - buffer
                }
                if vw.center.y < buffer {
                    vw.center.y = buffer
                }
            }
            
        }
        
    }
    
    func viewCoordinatesToImageCoordinates(pt:CGPoint) -> CGPoint{
        
        // ASUMMPTION: image height is 1200
        let imageRatio:CGFloat = selectedImage.size.width / selectedImage.size.height
        let viewRatio:CGFloat  = self.view.frame.width / self.view.frame.height
        let imgWidth:CGFloat = 1200 * imageRatio
        //let imgHeight:CGFloat = 1200
        let viewWidth:CGFloat  = self.view.frame.width
        //let viewHeight:CGFloat = self.view.frame.height //viewWidth / imageRatio
        //let x0:CGFloat = 0
        //let y0:CGFloat = self.view.center.y / 2
        //let outPt:CGPoint = CGPointMake((pt.x - x0) * imgWidth / viewWidth, (pt.y - y0) * imgHeight / viewHeight)
        let x0:CGFloat = origin.x
        let y0:CGFloat = origin.y
        
        if imageRatio > viewRatio {
            return CGPointMake((pt.x - x0) * imgWidth / viewWidth, (pt.y - y0) * imgWidth / viewWidth)
        } else {
            return CGPointMake((pt.x - x0) * imgWidth / viewWidth, (pt.y - y0) * imgWidth / viewWidth)
        }
        
    }
    
    
    @IBAction func okPressed(sender: AnyObject) {
        //print("okokokokok")
    }
    
    override func viewDidLayoutSubviews() {
        //print("view did layout subview")
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    //
    // MARK: - Navigation
    //
    
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        // code
        if (segue.identifier == "gotoDPVC"){
            
            // DO TRANSFORMATION
            
            // corners at view Coordinates (in imageView)
            let vtl :CGPoint = upperLeftCornerView.center   // point top-left
            let vtr :CGPoint = upperRightCornerView.center  // point top-right
            let vbl :CGPoint = lowerLeftCornerView.center   // point buttom-left
            let vbr :CGPoint = lowerRightCornerView.center  // point buttom-right
            // corners at mage Coordinates (in selectedImage)
            let ptl :CGPoint = viewCoordinatesToImageCoordinates(vtl)
            let ptr :CGPoint = viewCoordinatesToImageCoordinates(vtr)
            let pbl :CGPoint = viewCoordinatesToImageCoordinates(vbl)
            let pbr :CGPoint = viewCoordinatesToImageCoordinates(vbr)
            
            
            // Use OpenCV for image correction
            let inputs:NSArray = NSArray(objects:ptl.x, ptl.y, ptr.x, ptr.y, pbl.x, pbl.y,pbr.x, pbr.y)
            outImage = DataPickerOpenCV.cvGetTransferedImage(selectedImage, withRect: inputs as [AnyObject])
            
            let DPVC: DataPickerViewController = segue.destinationViewController as! DataPickerViewController
            DPVC.selectedImage = self.outImage
            
            // imageView.frame coordinates
            
            DPVC.xminPt = CGPointMake(lowerLeftCornerView.center.x + 10 - origin.x,lowerLeftCornerView.center.y - origin.y)
            DPVC.xmaxPt = CGPointMake(lowerRightCornerView.center.x - origin.x, lowerLeftCornerView.center.y - origin.y)
            DPVC.yminPt = CGPointMake(lowerLeftCornerView.center.x - origin.x, lowerLeftCornerView.center.y-10 - origin.y)
            DPVC.ymaxPt = CGPointMake(lowerLeftCornerView.center.x - origin.x, upperLeftCornerView.center.y - origin.y)
            DPVC.plotType = self.plotType
            
        }
    }




}
