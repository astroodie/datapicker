//
//  MagnifierView.swift
//  DataPicker
//
//  Created by Kuo-Chuan Pan on 7/4/15.
//  Copyright © 2015 Kuo-Chuan Pan. All rights reserved.
//

import Foundation
import UIKit
import MobileCoreServices


class MagnifierView: UIView {
    
    var touchPoint = CGPointMake(0,300)
    var viewToMagnify = UIView()
    
    required init?(coder aDecoder: NSCoder){
        super.init(coder: aDecoder)
    }
    
    override init(frame: CGRect){
        super.init(frame: frame)
        layer.borderColor = UIColor.lightGrayColor().CGColor
        layer.borderWidth = 2
        layer.cornerRadius = 50
        layer.masksToBounds = true
        //var viewToMagnify = UIView()
        
    }
    
    func setMyTouchPoint (pt: CGPoint) {
        self.touchPoint = pt
        self.center = CGPointMake(pt.x, pt.y - 90)
        
    }
    override func drawRect(rect: CGRect) {
        let context:CGContextRef = UIGraphicsGetCurrentContext()!
        CGContextSaveGState(context)
        CGContextTranslateCTM(context, (self.frame.size.width * 0.5), (self.frame.size.height * 0.5))
        CGContextScaleCTM(context, 1.5, 1.5)
        CGContextTranslateCTM(context, -touchPoint.x, -touchPoint.y)
        self.viewToMagnify.layer.renderInContext(context)
        
    }
    
}