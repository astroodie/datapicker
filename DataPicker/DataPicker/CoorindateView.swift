//
//  CoorindateView.swift
//  DataPicker
//
//  Created by Kuo-Chuan Pan on 7/5/15.
//  Copyright © 2015 Kuo-Chuan Pan. All rights reserved.
//

import UIKit

protocol CoordinateViewDataSource: class {
    func xminForCoordinateView(sender: CoordinateView) -> CGPoint?
    func xmaxForCoordinateView(sender: CoordinateView) -> CGPoint?
    func yminForCoordinateView(sender: CoordinateView) -> CGPoint?
    func ymaxForCoordinateView(sender: CoordinateView) -> CGPoint?
}

class CoordinateView: UIView {
    
    var xminPt:CGPoint!{
        didSet{
            self.setNeedsDisplay()
        }
    }
    var xmaxPt:CGPoint!{
        didSet{
            self.setNeedsDisplay()
        }
    }
    var yminPt:CGPoint!{
        didSet{
            self.setNeedsDisplay()
        }
    }
    var ymaxPt:CGPoint!{
        didSet{
            self.setNeedsDisplay()
        }
    }

    weak var dataSource: CoordinateViewDataSource?

    var xminText: UITextField!
    var xmaxText: UITextField!
    var yminText: UITextField!
    var ymaxText: UITextField!
    
    // Only override drawRect: if you perform custom drawing.
    // An empty implementation adversely affects performance during animation.
    override func drawRect(rect: CGRect) {
        // Drawing code
        
        xminPt = dataSource?.xminForCoordinateView(self) ?? CGPointMake(50, 50)
        xmaxPt = dataSource?.xmaxForCoordinateView(self) ?? CGPointMake(250, 50)
        yminPt = dataSource?.yminForCoordinateView(self) ?? CGPointMake(50, 250)
        ymaxPt = dataSource?.ymaxForCoordinateView(self) ?? CGPointMake(250, 250)
        
        
        // draw
        
        let marker: UIBezierPath = UIBezierPath()
        let lineColor = UIColor.greenColor()
        lineColor.set()
        marker.lineWidth = 2
        marker.moveToPoint(xminPt)
        marker.addLineToPoint(xmaxPt)
        marker.stroke()
        
        marker.moveToPoint(yminPt)
        marker.addLineToPoint(ymaxPt)
        marker.stroke()
        
        // xmin
        marker.moveToPoint(CGPointMake(xminPt.x, xminPt.y-10))
        marker.addLineToPoint(CGPointMake(xminPt.x, xminPt.y+10))
        marker.stroke()
        // xmax
        marker.moveToPoint(CGPointMake(xmaxPt.x, xmaxPt.y-10))
        marker.addLineToPoint(CGPointMake(xmaxPt.x, xmaxPt.y+10))
        marker.stroke()
        // ymin
        marker.moveToPoint(CGPointMake(yminPt.x-10, yminPt.y))
        marker.addLineToPoint(CGPointMake(yminPt.x+10, yminPt.y))
        marker.stroke()
        // ymax
        marker.moveToPoint(CGPointMake(ymaxPt.x-10, ymaxPt.y))
        marker.addLineToPoint(CGPointMake(ymaxPt.x+10, ymaxPt.y))
        marker.stroke()
        
        // draw text
        xminText.center = CGPointMake(xminPt.x, xminPt.y + 20)
        xmaxText.center = CGPointMake(xmaxPt.x, xmaxPt.y + 20)
        yminText.center = CGPointMake(yminPt.x - 30, yminPt.y)
        ymaxText.center = CGPointMake(ymaxPt.x - 30, ymaxPt.y)
        
    }
    
    override init(frame: CGRect) {
        
        super.init(frame: frame)
        
        
        self.backgroundColor = UIColor.clearColor()
        
        xminText = UITextField(frame: CGRectMake(0,0, 50, 30))
        xminText.text = "x1"
        xminText.textColor = UIColor.greenColor()
        xminText.userInteractionEnabled = false
        self.addSubview(xminText)
        
        xmaxText = UITextField(frame: CGRectMake(0,0, 50, 30))
        xmaxText.text = "x2"
        xmaxText.textColor = UIColor.greenColor()
        xmaxText.userInteractionEnabled = false
        self.addSubview(xmaxText)
        
        yminText = UITextField(frame: CGRectMake(0,0, 50, 30))
        yminText.text = "y1"
        yminText.textColor = UIColor.greenColor()
        yminText.userInteractionEnabled = false
        self.addSubview(yminText)
        
        ymaxText = UITextField(frame: CGRectMake(0,0, 50, 30))
        ymaxText.text = "y2"
        ymaxText.textColor = UIColor.greenColor()
        ymaxText.userInteractionEnabled = false
        self.addSubview(ymaxText)
    }
    
    required init(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }

}
