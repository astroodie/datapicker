//
//  CornerView.swift
//  DataPicker
//
//       USE:  the corner of corner control points
//
//  Created by Kuo-Chuan Pan on 7/3/15.
//  Copyright © 2015 Kuo-Chuan Pan. All rights reserved.
//

import UIKit

protocol CornerViewDelegate: class{
    func cornerTapped(center:CGPoint)
    func cornerChanged(center:CGPoint)
    func cornerFinished(center:CGPoint)
}

class CornerView: UIView {
    
    var delegate:CornerViewDelegate?
    
    var lastLocation:CGPoint = CGPointMake(0, 0)
    
    
    /*
    // Only override drawRect: if you perform custom drawing.
    // An empty implementation adversely affects performance during animation.
    override func drawRect(rect: CGRect) {
        // Drawing code
    }
    */
    
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        
        let panRecognizer = UIPanGestureRecognizer(target:self, action:"detectPan:")
        self.gestureRecognizers = [panRecognizer]
        
        layer.cornerRadius = 10
        self.backgroundColor = UIColor.redColor()
        self.alpha = 0.4
        
    }

    required init(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    func detectPan(recognizer:UIPanGestureRecognizer) {
        let translation  = recognizer.translationInView(self.superview!)
        self.center = CGPointMake(lastLocation.x + translation.x, lastLocation.y + translation.y)
        self.delegate?.cornerChanged(center)
        self.setNeedsDisplay()
        
        if recognizer.state == UIGestureRecognizerState.Ended {
            //print("pan ended")
            self.delegate?.cornerFinished(self.center)
        }
    }
    
    override func touchesBegan(touches: Set<UITouch>, withEvent event: UIEvent?) {
        self.superview?.bringSubviewToFront(self)
        self.delegate?.cornerTapped(self.center)
        lastLocation = self.center
    }
    
    override func touchesEnded(touches: Set<UITouch>, withEvent event: UIEvent?) {
        self.delegate?.cornerFinished(self.center)
    }
    

}
