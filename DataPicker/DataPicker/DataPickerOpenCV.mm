//
//  DataPickerOpenCV.m
//  DataPicker
//
//  Created by Kuo-Chuan Pan on 7/3/15.
//  Copyright © 2015 Kuo-Chuan Pan. All rights reserved.
//


#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>
#import "DataPickerOpenCV-Bridging-Header.h"
#import <opencv2/opencv.hpp>
#import <opencv2/highgui/ios.h>
#import <opencv2/imgproc/imgproc.hpp>
#import <iostream>
#include <stdlib.h>
#include <stdio.h>


@implementation DataPickerOpenCV : NSObject

+(UIImage *)cvDetectEdgeWithImage:(UIImage *)image{
    
    //UIImag to cv::Mat
    cv::Mat mat;
    UIImageToMat(image, mat);
    
    // cv: change to gray scale
    cv::Mat gray;
    cv::cvtColor(mat, gray, CV_BGR2GRAY);
    
    // cv: detect edge
    cv::Mat edge;
    cv::Canny(gray, edge, 200, 180);
    
    //cv::Mat to UIImage
    UIImage *edgeImg = MatToUIImage(edge);
    
    return edgeImg;
}

+(UIImage *)cvGetGrayImage:(UIImage *)image{
    
    cv::Mat mat;
    UIImageToMat(image, mat);
    
    
    cv::Mat gray;
    cv::cvtColor(mat, gray, CV_BGR2GRAY);
    
    UIImage *grayImg = MatToUIImage(gray);
    return grayImg;
}


+(NSArray *) cvGetRect:(UIImage *)image{
    
    
    
    BOOL success = NO;
    
    int thresh = 80;
    cv::Point2f center(0,0);
    cv::Mat src, ssrc, thr, mask;
    
    UIImageToMat(image, ssrc);
    
    double imageRatio = double(ssrc.cols) / double(ssrc.rows);
    // resize the image
    
    int imgWidth, imgHeight;
    
    imgWidth = 1200 * imageRatio;
    imgHeight = 1200;
    
    cv::Size size(imgWidth, imgHeight);
    cv::resize(ssrc, src, size);
    
    cv::cvtColor(src, thr, CV_BGR2GRAY);
    cv::blur(thr, thr, cv::Size(3,3));
    cv::Canny(thr, thr, thresh, thresh * 2,3);
    
    
    // Vector for storing contour
    cv::vector< cv::vector <cv::Point> > contours;
    cv::vector< cv::Vec4i > hierarchy;
    int largest_contour_index=0;
    int largest_area=0;
    
    // Create destination image
    //cv::Mat dst(src.rows,src.cols,CV_8UC1,cv::Scalar::all(0));
    
    // Find the contours in the image
    cv::findContours( thr.clone(), contours, hierarchy,CV_RETR_EXTERNAL, CV_CHAIN_APPROX_SIMPLE );
    
    for( int i = 0; i< contours.size(); i++ ){
        
        cv::vector<cv::vector<cv::Point> > contours_poly(1);
        cv::approxPolyDP( cv::Mat(contours[i]), contours_poly[0],10, true );
        if(contours_poly[0].size()==4){
            
            //std::cout << "a rect ..." << std::endl;
            double a=cv::contourArea( contours[i],false);  //  Find the area of contour
            if(a>largest_area){
                largest_area=a;
                largest_contour_index=i;                //Store the index of largest contour
            }
            
        }
    }
    
    //cv::drawContours( dst,contours, largest_contour_index, cv::Scalar(255,255,255),CV_FILLED, 8, hierarchy );
    cv::vector<cv::vector<cv::Point> > contours_poly(1);
    cv::approxPolyDP( cv::Mat(contours[largest_contour_index]), contours_poly[0],10, true );
    cv::Rect boundRect=boundingRect(contours[largest_contour_index]);
    //cv::Mat transformed = cv::Mat::zeros(src.rows, src.cols, CV_8UC3);
    
    CGPoint pt_tl = CGPointMake(0,0);
    CGPoint pt_bl = CGPointMake(0,0);
    CGPoint pt_br = CGPointMake(0,0);
    CGPoint pt_tr = CGPointMake(0,0);
    
    if(contours_poly[0].size()==4){
        
        
        // Get center of mass
        for (int i = 0; i < contours_poly[0].size(); i++){
            center.x += contours_poly[0][i].x;
            center.y += contours_poly[0][i].y;
        }
        center.x *= (1. / contours_poly[0].size());
        center.y *= (1. / contours_poly[0].size());
        //
        
        //sort corners
        std::vector<cv::Point2f> top, bot;
        
        for (int i = 0; i < contours_poly[0].size(); i++)
        {
            if (contours_poly[0][i].y < center.y)
                top.push_back(contours_poly[0][i]);
            else
                bot.push_back(contours_poly[0][i]);
        }
        
        cv::Point2f tl = top[0].x > top[1].x ? top[1] : top[0];
        cv::Point2f tr = top[0].x > top[1].x ? top[0] : top[1];
        cv::Point2f bl = bot[0].x > bot[1].x ? bot[1] : bot[0];
        cv::Point2f br = bot[0].x > bot[1].x ? bot[0] : bot[1];
        
        contours_poly[0].clear();
        contours_poly[0].push_back(tl);
        contours_poly[0].push_back(bl);
        contours_poly[0].push_back(br);
        contours_poly[0].push_back(tr);
        //
        
        // push corners and do perspective transform
        std::vector<cv::Point2f> quad_pts;
        std::vector<cv::Point2f> squre_pts;
        quad_pts.push_back(cv::Point2f(contours_poly[0][0].x,contours_poly[0][0].y));
        quad_pts.push_back(cv::Point2f(contours_poly[0][1].x,contours_poly[0][1].y));
        quad_pts.push_back(cv::Point2f(contours_poly[0][3].x,contours_poly[0][3].y));
        quad_pts.push_back(cv::Point2f(contours_poly[0][2].x,contours_poly[0][2].y));
        squre_pts.push_back(cv::Point2f(boundRect.x,boundRect.y));
        squre_pts.push_back(cv::Point2f(boundRect.x,boundRect.y+boundRect.height));
        squre_pts.push_back(cv::Point2f(boundRect.x+boundRect.width,boundRect.y));
        squre_pts.push_back(cv::Point2f(boundRect.x+boundRect.width,boundRect.y+boundRect.height));
        //cv::Mat transmtx = getPerspectiveTransform(quad_pts,squre_pts);
        //cv::warpPerspective(src, transformed, transmtx, src.size());
        //
        
        // Draw points/lines/rectangles
        /*
         cv::Point P1=contours_poly[0][0];
         cv::Point P2=contours_poly[0][1];
         cv::Point P3=contours_poly[0][2];
         cv::Point P4=contours_poly[0][3];
         
         cv::line(src,P1,P2, cv::Scalar(255,0,0),10,CV_AA,0);
         cv::line(src,P2,P3, cv::Scalar(0,255,255),10,CV_AA,0);
         cv::line(src,P3,P4, cv::Scalar(0,0,255),10,CV_AA,0);
         cv::line(src,P4,P1, cv::Scalar(0,0,255),10,CV_AA,0);
         cv::rectangle(src,boundRect,cv::Scalar(0,255,0),5,8,0);
         */
        //cv::rectangle(thr,boundRect,cv::Scalar(0,255,0),5,8,0);
        //cv::rectangle(transformed,boundRect,cv::Scalar(0,255,0),5,8,0);
        //cv::circle(src, P1, 10, CV_RGB(0,0,255), 5,8);
        
        //std::cout << "Found a rect" << std::endl;
        
        pt_tl = CGPointMake(tl.x, tl.y);
        pt_tr = CGPointMake(tr.x, tr.y);
        pt_bl = CGPointMake(bl.x, bl.y);
        pt_br = CGPointMake(br.x, br.y);
        
        success = YES;
        
        
    } else {
        // warning
        std::cout << "Ooooppps" << std::endl;
    }
    if (largest_area < (0.1 * src.rows * src.cols)) {
        success = NO;
    }
    
    UIImage *outImg = MatToUIImage(src);
    NSNumber * gotIt = [NSNumber numberWithBool:success];
    
    NSNumber * bx = [NSNumber numberWithFloat:boundRect.x];
    NSNumber * by = [NSNumber numberWithFloat:boundRect.y];
    NSNumber * bw = [NSNumber numberWithFloat:boundRect.width];
    NSNumber * bh = [NSNumber numberWithFloat:boundRect.height];
    
    NSValue * ptl = [NSValue valueWithCGPoint:pt_tl];
    NSValue * ptr = [NSValue valueWithCGPoint:pt_tr];
    NSValue * pbl = [NSValue valueWithCGPoint:pt_bl];
    NSValue * pbr = [NSValue valueWithCGPoint:pt_br];
    
    NSArray *results = [NSArray arrayWithObjects: outImg, gotIt, bx,by,bw,bh,ptl,ptr,pbl,pbr, nil];
    return [results copy];
}

+(UIImage *)cvGetTransferedImage:(UIImage *) image withRect:(NSArray *) inputs {
    
    cv::Mat src, ssrc;
    UIImageToMat(image, ssrc);
    // resize the image
    double imageRatio = double(ssrc.cols) / double(ssrc.rows);
    
    cv::Size size(int(1200 * imageRatio),int(1200));
    cv::resize(ssrc, src, size);
    cv::Mat transformed = cv::Mat::zeros(src.rows, src.cols, CV_8UC3);
    
    //
    cv::Rect boundRect;
    cv::Point2f tl, tr,bl,br;
    tl.x = [inputs[0] floatValue];
    tl.y = [inputs[1] floatValue];
    tr.x = [inputs[2] floatValue];
    tr.y = [inputs[3] floatValue];
    bl.x = [inputs[4] floatValue];
    bl.y = [inputs[5] floatValue];
    br.x = [inputs[6] floatValue];
    br.y = [inputs[7] floatValue];
    
    // new transformed bound box
    float boundRatio = (br.x - bl.x)/(bl.y -tl.y);
    
    boundRect.x = 75; //241;  // space for tics
    boundRect.y = 75; //324;
    
    if (boundRatio < 1) {
        // height > width
        boundRect.width = int(br.x - bl.x);
        boundRect.height = int(bl.y - tl.y);
    } else {
        // width > height
        boundRect.width = int(br.x - bl.x);
        boundRect.height = int(bl.y - tl.y);
    }
    
    float widthA, widthB, heightA, heightB, maxWidth, maxHeight;
    widthA = sqrt((br.x - bl.x) * (br.x - bl.x) + (br.y - bl.y) * (br.y - bl.y));
    widthB = sqrt((tr.x - tl.x) * (tr.x - tl.x) + (tr.y - tl.y) * (tr.y - tl.y));
    heightA = sqrt((tr.x - br.x) * (tr.x - br.x) + (tr.y - br.y) * (tr.y - br.y));
    heightB = sqrt((tl.x - bl.x) * (tl.x - bl.x) + (tl.y - bl.y) * (tl.y - bl.y));
    maxWidth = fmax(widthA, widthB)  + 150;
    maxHeight = fmax(heightA,heightB)  + 150;
    cv::Size maxSize(maxWidth,maxHeight);
    
    // push corners and do perspective transform
    std::vector<cv::Point2f> quad_pts;
    std::vector<cv::Point2f> squre_pts;
    quad_pts.push_back(cv::Point2f(tl.x,tl.y));
    quad_pts.push_back(cv::Point2f(bl.x,bl.y));
    quad_pts.push_back(cv::Point2f(tr.x,tr.y));
    quad_pts.push_back(cv::Point2f(br.x,br.y));
    squre_pts.push_back(cv::Point2f(boundRect.x,boundRect.y));
    squre_pts.push_back(cv::Point2f(boundRect.x,boundRect.y+boundRect.height));
    squre_pts.push_back(cv::Point2f(boundRect.x+boundRect.width,boundRect.y));
    squre_pts.push_back(cv::Point2f(boundRect.x+boundRect.width,boundRect.y+boundRect.height));
    cv::Mat transmtx = getPerspectiveTransform(quad_pts,squre_pts);
    //cv::warpPerspective(src, transformed, transmtx, src.size());
    cv::warpPerspective(src, transformed, transmtx, maxSize);
    cv::resize(transformed,src,size);
    UIImage *outImg = MatToUIImage(src);
    return outImg;
}

+(UIImage *) cvGetPlotImage:(UIImage *)image{
    
    // REF 1: http://stackoverflow.com/questions/22519545/automatic-perspective-correction-opencv
    // REF 2: http://opencv-code.com/tutorials/automatic-perspective-correction-for-quadrilateral-objects/
    // REF 2: https://github.com/bsdnoobz/opencv-code/blob/master/quad-segmentation.cpp
    
    
    int thresh = 80;
    cv::Point2f center(0,0);
    
    cv::Mat src, ssrc,bw, thr, mask;
    UIImageToMat(image, ssrc);
    
    // resize the image
    if (ssrc.cols > 1200) {
        cv::Size size(1200,1200);
        cv::resize(ssrc, src, size);
    } else {
        src = ssrc.clone();
    }
    
    cv::cvtColor(src, bw, CV_BGR2GRAY);
    cv::blur(bw, bw, cv::Size(3,3));
    cv::Canny(bw, thr, thresh, thresh * 2,3);
    //cv::threshold(bw, thr, 127, 255, CV_THRESH_BINARY);
    
    
    // Vector for storing contour
    cv::vector< cv::vector <cv::Point> > contours;
    cv::vector< cv::Vec4i > hierarchy;
    int largest_contour_index=0;
    int largest_area=0;
    
    // Create destination image
    cv::Mat dst(src.rows,src.cols,CV_8UC1,cv::Scalar::all(0));
    
    // Find the contours in the image
    cv::findContours( thr.clone(), contours, hierarchy,CV_RETR_EXTERNAL, CV_CHAIN_APPROX_SIMPLE );
    for( int i = 0; i< contours.size(); i++ ){
        
        cv::vector<cv::vector<cv::Point> > contours_poly(1);
        cv::approxPolyDP( cv::Mat(contours[i]), contours_poly[0],10, true );
        if(contours_poly[0].size()==4){
            
            //std::cout << "a rect ..." << std::endl;
            double a=cv::contourArea( contours[i],false);  //  Find the area of contour
            if(a>largest_area){
                largest_area=a;
                largest_contour_index=i;                //Store the index of largest contour
            }
            
        }
    }
    
    cv::drawContours( dst,contours, largest_contour_index, cv::Scalar(255,255,255),CV_FILLED, 8, hierarchy );
    cv::vector<cv::vector<cv::Point> > contours_poly(1);
    cv::approxPolyDP( cv::Mat(contours[largest_contour_index]), contours_poly[0],10, true );
    cv::Rect boundRect=boundingRect(contours[largest_contour_index]);
    cv::Mat transformed = cv::Mat::zeros(src.rows, src.cols, CV_8UC3);
    
    
    //cgBoundRect = CGRectMake(boundRect.x, boundRect.y, boundRect.width, boundRect.height);
    
    if(contours_poly[0].size()==4){
        
        
        // Get center of mass
        for (int i = 0; i < contours_poly[0].size(); i++){
            center.x += contours_poly[0][i].x;
            center.y += contours_poly[0][i].y;
        }
        center.x *= (1. / contours_poly[0].size());
        center.y *= (1. / contours_poly[0].size());
        //
        
        //sort corners
        std::vector<cv::Point2f> top, bot;
        
        for (int i = 0; i < contours_poly[0].size(); i++)
        {
            if (contours_poly[0][i].y < center.y)
                top.push_back(contours_poly[0][i]);
            else
                bot.push_back(contours_poly[0][i]);
        }
        
        cv::Point2f tl = top[0].x > top[1].x ? top[1] : top[0];
        cv::Point2f tr = top[0].x > top[1].x ? top[0] : top[1];
        cv::Point2f bl = bot[0].x > bot[1].x ? bot[1] : bot[0];
        cv::Point2f br = bot[0].x > bot[1].x ? bot[0] : bot[1];
        contours_poly[0].clear();
        contours_poly[0].push_back(tl);
        contours_poly[0].push_back(bl);
        contours_poly[0].push_back(br);
        contours_poly[0].push_back(tr);
        //
        
        // push corners and do perspective transform
        std::vector<cv::Point2f> quad_pts;
        std::vector<cv::Point2f> squre_pts;
        quad_pts.push_back(cv::Point2f(contours_poly[0][0].x,contours_poly[0][0].y));
        quad_pts.push_back(cv::Point2f(contours_poly[0][1].x,contours_poly[0][1].y));
        quad_pts.push_back(cv::Point2f(contours_poly[0][3].x,contours_poly[0][3].y));
        quad_pts.push_back(cv::Point2f(contours_poly[0][2].x,contours_poly[0][2].y));
        squre_pts.push_back(cv::Point2f(boundRect.x,boundRect.y));
        squre_pts.push_back(cv::Point2f(boundRect.x,boundRect.y+boundRect.height));
        squre_pts.push_back(cv::Point2f(boundRect.x+boundRect.width,boundRect.y));
        squre_pts.push_back(cv::Point2f(boundRect.x+boundRect.width,boundRect.y+boundRect.height));
        cv::Mat transmtx = getPerspectiveTransform(quad_pts,squre_pts);
        cv::warpPerspective(src, transformed, transmtx, src.size());
        //
        
        // Draw points/lines/rectangles
        cv::Point P1=contours_poly[0][0];
        cv::Point P2=contours_poly[0][1];
        cv::Point P3=contours_poly[0][2];
        cv::Point P4=contours_poly[0][3];
        
        cv::line(src,P1,P2, cv::Scalar(255,0,0),10,CV_AA,0);
        cv::line(src,P2,P3, cv::Scalar(0,255,255),10,CV_AA,0);
        cv::line(src,P3,P4, cv::Scalar(0,0,255),10,CV_AA,0);
        cv::line(src,P4,P1, cv::Scalar(0,0,255),10,CV_AA,0);
        cv::rectangle(src,boundRect,cv::Scalar(0,255,0),5,8,0);
        cv::rectangle(thr,boundRect,cv::Scalar(0,255,0),5,8,0);
        cv::rectangle(transformed,boundRect,cv::Scalar(0,255,0),5,8,0);
        
        cv::circle(src, P1, 10, CV_RGB(0,0,255), 5,8);
        
        //std::cout << "Found a rect" << std::endl;
        
    } else {
        // warning
        std::cout << "Ooooppps" << std::endl;
    }
    //UIImage *srcImg = MatToUIImage(src);
    
    //cv::Rect plotRegion(boundRect.x,boundRect.y,boundRect.width,boundRect.height);
    //cv::Mat croppedImage = transformed(plotRegion);
    
    UIImage *outImg = MatToUIImage(src);
    return outImg;
}

+(UIImage *)cvTest:(UIImage *)image{
    cv::Mat mat;
    cv::Mat outMat;
    UIImageToMat(image, mat);
    
    // ------simple random mat image
    //cv::Mat R= cv::Mat(30,20,CV_8UC3);
    //cv::randu(R, cv::Scalar::all(0), cv::Scalar::all(255));
    
    
    // -------filter 2D: enhance contrast
    
    //cv::Mat kern = (cv::Mat_<char>(3,3) << 0, 01, 0, -1, 5, -1, 0, -1, 0);
    //cv::filter2D(mat, outMat, mat.depth(), kern);
    
    
    // :scaling g(i,j) = alpha * f (i,j) + beta
    // ex. alpha = 2.2,  beta = 50
    //mat.convertTo(outMat, -1, 2.2, 50);
    
    //--------- draw
    // http://docs.opencv.org/doc/tutorials/core/basic_geometric_drawing/basic_geometric_drawing.html
    //
    /*
     cv::Mat draw = cv::Mat::zeros(300, 300, CV_8UC3);
     cv::ellipse(draw, cv::Point(150,150), cv::Size(50,25), 90, 0, 360, cv::Scalar(255,0,0),1, 8);
     cv::ellipse(draw, cv::Point(150,150), cv::Size(50,25), 0, 0, 360, cv::Scalar(255,0,0),1, 8);
     cv::ellipse(draw, cv::Point(150,150), cv::Size(50,25), 45, 0, 360, cv::Scalar(255,0,0),1, 8);
     cv::ellipse(draw, cv::Point(150,150), cv::Size(50,25),-45, 0, 360, cv::Scalar(255,0,0),1, 8);
     */
    
    
    
    //-------
    // detect Line: http://docs.opencv.org/doc/tutorials/imgproc/imgtrans/hough_lines/hough_lines.html#hough-lines
    //
    
    cv::Mat dst, cdst;
    
    cv::Canny(mat, dst, 50, 200, 3);
    cv::cvtColor(dst, cdst, CV_GRAY2BGR);
    
    cv::Mat gray, edge;
    cv::cvtColor(mat, gray, CV_BGR2GRAY);
    cv::Canny(gray, edge, 200, 180);
    
    /* // -------     Not working
     cv::vector<cv::Vec2f> lines;
     cv::HoughLines(dst, lines, 1, CV_PI/180, 100);
     
     for (size_t i=0; i < lines.size(); i++){
     float rho = lines[i][0], theta = lines[i][1];
     cv::Point pt1, pt2;
     double a = cos(theta), b= sin(theta);
     double x0 = a * rho, y0 = b * rho;
     pt1.x = cvRound(x0 + 1000 * (-b));
     pt1.y = cvRound(y0 + 1000 * (a));
     pt2.x = cvRound(x0 - 1000 * (-b));
     pt2.y = cvRound(y0 - 1000 * (a));
     cv::line(cdst, pt1, pt2, cv::Scalar(0,0,255),3, CV_AA);
     }
     */
    
    cv::Mat lineFrame = cv::Mat::zeros(cdst.rows, cdst.cols, CV_8UC3);
    
    //-------    working good!
    
    cv::vector<cv::Vec4i> lines;
    //HoughLinesP(dst,lines,rho resolution, theta resolution, threshold, minLineLength, maxLinesGap)
    cv::HoughLinesP(edge, lines, 2, CV_PI/180, 100,50,20);
    float lineWidth;
    for (size_t i=0; i < lines.size(); i++){
        cv::Vec4i l = lines[i];
        
        lineWidth = sqrt((l[2]-l[0])*(l[2]-l[0]) + (l[3]-l[1])*(l[3]-l[1]));
        cv::line(lineFrame, cv::Point(l[0],l[1]), cv::Point(l[2],l[3]), cv::Scalar(0,0,255),3,CV_AA);
        
        // draw corners
    }
    
    
    
    // Bolb detection
    int imageCols = gray.cols;
    int imageRows = gray.rows;
    int majorSize = fmin(imageCols,imageRows);
    
    cv::SimpleBlobDetector::Params params;
    //params.minThreshold = 40;
    //params.maxThreshold = 60;
    //params.thresholdStep = 5;
    //params.minDistBetweenBlobs = 10.0;
    
    //params.filterByInertia = true;
    //params.minInertiaRatio = 0.01;
    
    //params.filterByConvexity = true;
    //params.minConvexity = 0.3;
    //params.maxConvexity = 100;
    
    params.filterByColor = false;
    //params.filterByCircularity = true;
    //params.minCircularity = 0.3;
    //params.maxCircularity = 0.9;
    
    params.filterByArea = true;
    params.minArea = majorSize * majorSize / 10;
    params.maxArea = majorSize * majorSize;
    
    cv::Ptr<cv::FeatureDetector> blob_detector = new cv::SimpleBlobDetector(params);
    blob_detector->create("SimpleBlob");
    
    // detect
    
    cv::vector<cv::KeyPoint> keypoints;
    
    
    blob_detector->detect(gray, keypoints);
    
    // extract the x-y coordinates of the keypoints
    for (int i=0; i<keypoints.size();i++){
        float X = keypoints[i].pt.x;
        float Y = keypoints[i].pt.y;
        
        //cv::circle(lineFrame, cv::Point(X,Y), keypoints[i].size, cv::Scalar(255,0,0),2, 8);
        cv::rectangle(lineFrame, cv::Point(X-(keypoints[i].size),Y-(keypoints[i].size)), cv::Point(X+(keypoints[i].size),Y+(keypoints[i].size)), cv::Scalar(255,0,0),3,8);
    }
    
    
    UIImage *outImg = MatToUIImage(lineFrame);
    return outImg;
}

@end

