//
//  DataPickerViewController.swift
//  DataPicker
//
//  Created by Kuo-Chuan Pan on 7/3/15.
//  Copyright © 2015 Kuo-Chuan Pan. All rights reserved.
//

import UIKit
import MobileCoreServices
import MessageUI
import AVFoundation

extension String {
    var floatValue: Float {
        return (self as NSString).floatValue
    }
}

class DataPickerViewController: UIViewController, UINavigationControllerDelegate, UIGestureRecognizerDelegate, UITextFieldDelegate, MFMailComposeViewControllerDelegate, RangeViewDelegate, CoordinateViewDataSource {

    //let clickSound = NSURL(fileURLWithPath: NSBundle.mainBundle().pathForResource("Click1", ofType: "m4a")!)
    
    //var audioPlayer = AVAudioPlayer()
    
    var magnifier : MagnifierView!{
        didSet{
            magnifier.superview?.bringSubviewToFront(magnifier)
        }
    }
    
    var plotType:Int = 1
    var hasCoordinate:Bool = false
    var currentAim:AimView?
    var ndata:Int = 0
    var isXlog:Bool = false
    var isYlog:Bool = false
    var origin:CGPoint! = CGPointMake(0, 0)
    
    var coordinateView : CoordinateView!{
        didSet{
            coordinateView.dataSource = self
        }
    }
    
    var selectedImage : UIImage!
    var xminPt: CGPoint! = CGPointMake(0, 200)
    var xmaxPt: CGPoint! = CGPointMake(200, 200)
    var yminPt: CGPoint! = CGPointMake(0, 200)
    var ymaxPt: CGPoint! = CGPointMake(0, 10)
    
    var xminView: RangeView!
    var xmaxView: RangeView!
    var yminView: RangeView!
    var ymaxView: RangeView!
    var resultString:String = ""

    
    //-----------------------------------------------------------------
    //
    //   EXPORT BUTTON
    //
    //-----------------------------------------------------------------
   
    @IBAction func exportButton(sender: AnyObject) {
        
        //
        // Check Data
        //
        var sucess:Bool = false
        var data:[CGPoint] = []
        var results:[CGPoint] = []
        
        
        
        //
        //   WARNINGS
        //
        
        if ndata <= 0 {
            // warning: no data
            sucess = false
            let alert: UIAlertController = UIAlertController(title:"Warning", message: "Empty data", preferredStyle:.Alert)
            let ok: UIAlertAction = UIAlertAction(title:"OK", style:.Default, handler: nil)
            alert.addAction(ok)
            
            // when using iPad
            alert.popoverPresentationController?.sourceView = sender as? UIView;
            // present the alert sheet
            self.presentViewController(alert, animated: true, completion: nil)
        }
        
        
        // add data and sort
        for view in imageView.subviews {
            if view is AimView{
                data.append(view.center)
            }
        }
        data = data.sort({$0.x < $1.x})
        sucess = true
        
        // prepare data transformation data
        var x1:CGFloat = CGFloat()
        var x2:CGFloat = CGFloat()
        var y1:CGFloat = CGFloat()
        var y2:CGFloat = CGFloat()
        
        if let tmp1:Float = x1TextField.text?.floatValue {
            x1 = CGFloat(tmp1)
        } else {
            sucess = false
        }
        if let tmp2:Float = x2TextField.text?.floatValue {
            x2 = CGFloat(tmp2)
        } else {
            sucess = false
        }
        if let tmp3:Float = y1TextField.text?.floatValue {
            y1 = CGFloat(tmp3)
        } else {
            sucess = false
        }
        if let tmp4:Float = y2TextField.text?.floatValue {
            y2 = CGFloat(tmp4)
        } else {
            sucess = false
        }
        
        
        if !sucess {
            
            // something wrong
            let alert: UIAlertController = UIAlertController(title:"Error", message: "Invalid inputs. Make sure x1, x2, y1, y2 are real numbers.", preferredStyle:.Alert)
            let ok: UIAlertAction = UIAlertAction(title:"OK", style:.Default, handler: nil)
            alert.addAction(ok)
            
            // when using iPad
            alert.popoverPresentationController?.sourceView = sender as? UIView;
            // present the alert sheet
            self.presentViewController(alert, animated: true, completion: nil)
        }
        
        // CONVERT DATA using DataPickerModel()
        
        let dp:DataPickerModel = DataPickerModel()
        dp.setMode(1)
        dp.dataPlot_setControlPoints(xminPt, vxmax: xmaxPt, vymin: yminPt, vymax: ymaxPt)
        dp.dataPlot_setLogs(isXlog, isYlog: isYlog)
        dp.dataPLot_setPhysicalPoints(x1, px2: x2, py1: y1, py2: y2)
        
        
        resultString = "#======================================\n"
        resultString += "#               X                   Y\n"
        
        for vpt in data {
            let ppt = dp.dataPlot_getPhysicalPoints(vpt)
            results.append(ppt)
            //let line = String(format:"  %+5.3E    %+5.3E \n", ppt.x, ppt.y)
            let line = "\(ppt.x)  \(ppt.y) \n"
            resultString += line
        }
        print(resultString)
        
        
        // ------------------------------------------------------------------------------------------
        // CREAT ACTIONS
        // ------------------------------------------------------------------------------------------
        
        let sheet: UIAlertController = UIAlertController(title: "Choose An Export Option", message: "", preferredStyle:.ActionSheet)
        
        let cancelAction: UIAlertAction = UIAlertAction(title: "Cancel", style: .Cancel, handler:nil)
        sheet.addAction(cancelAction)
        sheet.addAction(UIAlertAction(title: "Email To Myself", style: .Default){action -> Void in
            // email to ....
            print("email!!!!")
            
            let emailTitle = "[Data Picker] data "
            let messageBody = "Email sent from DataPicker \n \n \n" + self.resultString
            
            let toRecipents = ["astroodie@gmail.com"]
            
            if MFMailComposeViewController.canSendMail() {
                
                let mc:MFMailComposeViewController = MFMailComposeViewController()
                mc.mailComposeDelegate = self
                mc.setSubject(emailTitle)
                mc.setMessageBody(messageBody, isHTML: false)
                mc.setToRecipients(toRecipents)
                let imageData = UIImagePNGRepresentation(self.selectedImage)
                
                let date = NSDate()
                let calendar = NSCalendar.currentCalendar()
                let components = calendar.components(NSCalendarUnit.Hour, fromDate: date)
                let hour = components.hour
                let minutes = components.minute
                let fileTime = "image_\(date)_\(hour)_\(minutes).png"
                
                mc.addAttachmentData(imageData!, mimeType: "image", fileName: fileTime)
                
                self.presentViewController(mc, animated: true, completion: nil)
            } else {
                
                // warning: cannot send email
                sucess = false
                let alert: UIAlertController = UIAlertController(title:"Warning", message: "Your device cannot send emails", preferredStyle:.Alert)
                let ok: UIAlertAction = UIAlertAction(title:"OK", style:.Default, handler: nil)
                alert.addAction(ok)
                
                // when using iPad
                alert.popoverPresentationController?.sourceView = sender as? UIView;
                // present the alert sheet
                self.presentViewController(alert, animated: true, completion: nil)
                
            }
        })
        sheet.addAction(UIAlertAction(title:"Save to Dropbox", style:.Default){
            action -> Void in
            // do stuff
            print("export to dropbox")
            
        })
        sheet.addAction(UIAlertAction(title:"Show on screen", style:.Default){
            action -> Void in
            // do stuff
            print("show on screen")
            self.performSegueWithIdentifier("showResults", sender: self)
        })
        
        // when using iPad
        sheet.popoverPresentationController?.sourceView = sender as? UIView;
        // present the alert sheet
        self.presentViewController(sheet, animated: true, completion: nil)
        
    }
    
    //-----------------------------------------------------------------
    //
    func mailComposeController(controller: MFMailComposeViewController, didFinishWithResult result: MFMailComposeResult, error: NSError?) {
        switch result.rawValue {
            
        case MFMailComposeResultCancelled.rawValue:
        NSLog("Mail cancelled")
        case MFMailComposeResultSaved.rawValue:
        NSLog("Mail saved")
        case MFMailComposeResultSent.rawValue:
        NSLog("Mail sent")
        case MFMailComposeResultFailed.rawValue:
        NSLog("Mail sent failure:")
        default:
        print("Whooops....")
            //break
        }
        self.dismissViewControllerAnimated(false, completion: nil)
    }
    
    
    //-----------------------------------------------------------------
    //
    //   X SACLE
    //
    //-----------------------------------------------------------------
    
    @IBAction func isXlogChanged(sender: AnyObject) {
        if isXlog{
            isXlog = false
        } else {
            isXlog = true
        }
    }
    
    //-----------------------------------------------------------------
    //
    //   Y SACLE
    //
    //-----------------------------------------------------------------
    
    @IBAction func isYlogChanged(sender: AnyObject) {
        if isYlog{
            isYlog = false
        } else {
            isYlog = true
        }
    }
    
    @IBOutlet weak var x1TextField: UITextField!
    @IBOutlet weak var x2TextField: UITextField!
    @IBOutlet weak var y1TextField: UITextField!
    @IBOutlet weak var y2TextField: UITextField!
    
    
    //-----------------------------------------------------------------
    //
    //   UNDO BUTTON
    //
    //-----------------------------------------------------------------
    
    @IBAction func undoButton(sender: AnyObject) {
        //print("undo button pressed")
        for view in imageView.subviews {
            if view is AimView {
                if view.tag == ndata{
                    view.removeFromSuperview()
                    ndata -= 1
                }
            }
        }
    }
    
    //-----------------------------------------------------------------
    //
    //   ImageView
    //
    //-----------------------------------------------------------------
    
    
    @IBOutlet weak var imageView: UIImageView!
    
    //-----------------------------------------------------------------
    //
    //   RangeView DELEGATE
    //
    //-----------------------------------------------------------------
    
    func cornerTapped(center:CGPoint, axis:Int, limit:Int){
        updateUI()
    }
    func cornerChanged(center:CGPoint, axis:Int, limit:Int){
        
        if center.y <= imageView.frame.height - 10 && center.x <= imageView.frame.width - 10 {
            if axis == 1 {
                if limit == 1 {
                    // tapping xmin
                    xminPt = center
                    xmaxPt.y = center.y
                    xmaxView.center.y = center.y
                } else {
                    // tapping xmax
                    xmaxPt = center
                    xminPt.y = center.y
                    xminView.center.y = center.y
                }
            } else {
                if limit == 1 {
                    yminPt = center
                    ymaxPt.x = center.x
                    ymaxView.center.x = center.x
                } else {
                    ymaxPt = center
                    yminPt.x = center.x
                    yminView.center.x = center.x
                }
            }
            updateUI()
            
            // update Magnifier
            magnifier.setMyTouchPoint(center)
            magnifier.hidden = false
            magnifier.setNeedsDisplay()
            
        } else {
            //
        }
        
        
    }
    func cornerFinished(center:CGPoint, axis:Int, limit:Int){
        magnifier.hidden = true
    }
    
    func updateUI(){
        coordinateView.setNeedsDisplay()
        xminView.setNeedsDisplay()
        xmaxView.setNeedsDisplay()
        yminView.setNeedsDisplay()
        ymaxView.setNeedsDisplay()
        self.view.bringSubviewToFront(xminView)
        self.view.bringSubviewToFront(xmaxView)
        self.view.bringSubviewToFront(yminView)
        self.view.bringSubviewToFront(ymaxView)
        
    }
    
    // coodirnate data source 
    
    func xminForCoordinateView(sender: CoordinateView) -> CGPoint?{
        return xminPt
    }
    func xmaxForCoordinateView(sender: CoordinateView) -> CGPoint?{
        return xmaxPt
    }
    func yminForCoordinateView(sender: CoordinateView) -> CGPoint?{
        return yminPt
    }
    func ymaxForCoordinateView(sender: CoordinateView) -> CGPoint?{
        return ymaxPt
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        imageView.image = selectedImage
        imageView.contentMode = UIViewContentMode.ScaleAspectFit
        
        
        // prepar sound
        
        /*do {
            try audioPlayer = AVAudioPlayer(contentsOfURL: clickSound, fileTypeHint: nil)
        } catch {
            // handle error
        }
        audioPlayer.prepareToPlay() */
        


        // set Gesture
        let longPress = UILongPressGestureRecognizer(target: self, action: "longPressed:")
        longPress.delegate = self
        imageView.addGestureRecognizer(longPress)
        
        // draw magnifier
        let center = CGPointMake(0, 0)
        magnifier = MagnifierView(frame: CGRectMake(center.x-100,center.y-100,100,100))
        magnifier.viewToMagnify = imageView
        magnifier.hidden = true
        imageView.addSubview(magnifier)
        
        // text fields
        x1TextField.delegate = self
        x2TextField.delegate = self
        y1TextField.delegate = self
        y2TextField.delegate = self
        
    }

    
    override func viewDidAppear(animated: Bool) {
        //print("view did appear")
        
        if !hasCoordinate {
            
            hasCoordinate = true
            coordinateView = CoordinateView(frame: CGRectMake(0, 0, imageView.frame.width, imageView.frame.height))
            coordinateView.dataSource = self
            imageView.addSubview(coordinateView)
            
            //
            let imageRatio:CGFloat = selectedImage.size.width / selectedImage.size.height
            let viewRatio:CGFloat = imageView.frame.width / imageView.frame.height
            
            // the image size in imageview coordinate
            var outWidth:CGFloat  = 0
            var outHeight:CGFloat = 0
            
            if imageRatio >= viewRatio {
                // width > height
                outWidth  = imageView.frame.size.width
                outHeight = outWidth / imageRatio
                origin = CGPointMake(0, (imageView.frame.height - outHeight) / 2)
                
            } else {
                // height > width
                outHeight = imageView.frame.size.height
                outWidth  = outHeight * imageRatio
                origin = CGPointMake((imageView.frame.width - outWidth) / 2, 0)
            }
            
            xminPt = CGPointMake(max(xminPt.x + origin.x, 30), min(xminPt.y + origin.y, outHeight - 30))
            xmaxPt = CGPointMake(min(xmaxPt.x + origin.x, outWidth - 30), min(xmaxPt.y + origin.y, outHeight - 30))
            yminPt = CGPointMake(max(yminPt.x + origin.x, 30), min(yminPt.y + origin.y, outHeight - 30))
            ymaxPt = CGPointMake(max(ymaxPt.x + origin.x, 30), max(ymaxPt.y + origin.y, 30))
            
            // draw coordinates
            xminView = RangeView(frame: CGRectMake(xminPt.x - 10, xminPt.y - 10, 20, 20))
            xminView.axis = 1
            xminView.limit = 1
            xminView.xmax = imageView.frame.size.width - 10
            xminView.ymax = imageView.frame.size.height - 10
            imageView.addSubview(xminView)
            
            xmaxView = RangeView(frame: CGRectMake(xmaxPt.x - 10, xmaxPt.y - 10, 20, 20))
            xmaxView.axis = 1
            xmaxView.limit = 2
            xmaxView.xmax = imageView.frame.size.width - 10
            xmaxView.ymax = imageView.frame.size.height - 10
            imageView.addSubview(xmaxView)
            
            yminView = RangeView(frame: CGRectMake(yminPt.x - 10, yminPt.y - 10, 20, 20))
            yminView.axis = 2
            yminView.limit = 1
            yminView.xmax = imageView.frame.size.width - 10
            yminView.ymax = imageView.frame.size.height - 10
            imageView.addSubview(yminView)
            
            ymaxView = RangeView(frame: CGRectMake(ymaxPt.x - 10, ymaxPt.y - 10, 20, 20))
            ymaxView.axis = 2
            ymaxView.limit = 2
            ymaxView.xmax = imageView.frame.size.width - 10
            ymaxView.ymax = imageView.frame.size.height - 10
            imageView.addSubview(ymaxView)
            
            xminView.delegate = self
            xmaxView.delegate = self
            yminView.delegate = self
            ymaxView.delegate = self
            
            print("outHeight = \(outHeight)")
            print("image view width \(imageView.frame.width)")
            print("image view height \(imageView.frame.height)")
            print("Image width = \(imageView.image?.size.width)")
            print("Image hirght = \(imageView.image?.size.height)")
            
            // adjust corner points if exceed boundary
            let buffer:CGFloat = 20
            
            for vw in imageView.subviews {
                if vw is RangeView {
                    if vw.center.x > self.imageView.frame.width - buffer {
                        vw.center.x = self.imageView.frame.width - buffer
                    }
                    if vw.center.x < buffer {
                        vw.center.x = buffer
                    }
                    if vw.center.y > self.imageView.frame.height - buffer {
                        vw.center.y = self.imageView.frame.height - buffer
                    }
                    if vw.center.y < buffer {
                        vw.center.y = buffer
                    }
                }
                
            }
            
        }
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
    func longPressed (sender: UILongPressGestureRecognizer) {
        
        // get the coordinate in imageView
        let point = sender.locationOfTouch(0, inView: imageView)
        
        
        // update Magnifier
        magnifier.superview?.bringSubviewToFront(magnifier)
        magnifier.setMyTouchPoint(point)
        magnifier.setNeedsDisplay()
        
        // update aim
        currentAim?.setMyTouchPoint(point)
        currentAim?.setNeedsDisplay()
        
        
        if UIGestureRecognizerState.Ended == sender.state || UIGestureRecognizerState.Cancelled == sender.state || UIGestureRecognizerState.Failed == sender.state{
            
            // long press finished, do some stuff
            
            magnifier.hidden = true
            //audioPlayer.play()
            currentAim = nil
            
            print("Add a view point at \(point.x) \(point.y)")
            
        }
        if UIGestureRecognizerState.Began == sender.state {
            // long press starts
            
            magnifier.hidden = false
            
            // creat an aim
            ndata += 1
            let aim: AimView = AimView(frame: CGRectMake(point.x, point.y, 100, 100))
            aim.setMyTouchPoint(point)
            aim.tag = ndata
            imageView.addSubview(aim)
            currentAim = aim
            
            
        }
    }
    
    override func touchesBegan(touches: Set<UITouch>, withEvent event: UIEvent?) {
        //print("touches begain ...")
        x1TextField.resignFirstResponder()
        x2TextField.resignFirstResponder()
        y1TextField.resignFirstResponder()
        y2TextField.resignFirstResponder()
        
    }
    
    
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
        
        if (segue.identifier == "showResults"){
            let RVC: ResultsViewController = segue.destinationViewController as! ResultsViewController
            RVC.resultString = resultString
        }
    }

    func textFieldDidBeginEditing(textField: UITextField) {
        let newCenter = CGPointMake(view.center.x, view.center.y - 260) // was 166
        UIView.animateWithDuration(0.4, animations: { self.view.center = newCenter
            }, completion: nil)
        
    }
    func textFieldDidEndEditing(textField: UITextField) {
        let newCenter = CGPointMake(view.center.x, view.center.y + 260)
        UIView.animateWithDuration(0.4, animations: { self.view.center = newCenter
            }, completion: nil)
    }
    func textFieldShouldReturn(textField: UITextField) -> Bool {
        textField.resignFirstResponder()
        return true
    }


}
