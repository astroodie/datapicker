//
//  ViewController.swift
//  DataPicker
//
//  Created by Kuo-Chuan Pan on 7/3/15.
//  Copyright © 2015 Kuo-Chuan Pan. All rights reserved.
//

import UIKit

class ViewController: UIViewController, UIImagePickerControllerDelegate, UINavigationControllerDelegate {

    @IBOutlet weak var graphPlotButton: UIButton!
    
    var plotType:Int = 1
    var selectedImage : UIImage!
    var hasSelectedImage: Bool = false
    
    @IBAction func settingButtonPressed(sender: AnyObject) {
        
        
        /*
        if !DBSession.sharedSession().isLinked(){
            DBSession.sharedSession().linkFromController(self)
            print("trying to link dropbox")
        }
        
        if DBSession.sharedSession().isLinked(){
            //linkDropboxButton.setTitle("Dropbox linked", forState: UIControlState.Normal)
            //dropboxIconView.image = UIImage(named: "dropbox_linked")
            print("dropbox is linked")
        }*/
        
        
    }
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
        self.navigationController?.navigationBar.hidden = true
       
    }
    
    override func viewWillAppear(animated: Bool) {
        super.viewWillAppear(false)
        self.navigationController?.navigationBar.hidden = true
    }
    
    override func viewDidAppear(animated: Bool) {
        super.viewDidAppear(true)
        if hasSelectedImage {
            hasSelectedImage = false
            self.performSegueWithIdentifier("gotoEditView", sender: self)
        }
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func loadPhotoActions(sender: AnyObject){
        let sheet: UIAlertController = UIAlertController(title: "Please choose an option", message: "", preferredStyle:.ActionSheet)
        
        let cancelAction: UIAlertAction = UIAlertAction(title: "Cancel", style: .Cancel, handler:nil)
        sheet.addAction(cancelAction)
        
        let takePictureAction: UIAlertAction = UIAlertAction(title:"Take A Picture", style:.Default){action -> Void in
            // do stuff
            let imagePicker = UIImagePickerController()
            imagePicker.delegate = self
            imagePicker.sourceType = UIImagePickerControllerSourceType.Camera
            imagePicker.allowsEditing = false
            imagePicker.delegate = self
            self.presentViewController(imagePicker, animated: true, completion: nil)
        }
        sheet.addAction(takePictureAction)
        
        let loadPictureAction: UIAlertAction = UIAlertAction(title:"Choose From Camera Roll", style:.Default){action -> Void in
            // do stuff
            let imagePicker = UIImagePickerController()
            imagePicker.delegate = self
            imagePicker.sourceType = UIImagePickerControllerSourceType.PhotoLibrary
            imagePicker.allowsEditing = false
            imagePicker.delegate = self
            self.presentViewController(imagePicker, animated: true, completion: nil)
        }
        sheet.addAction(loadPictureAction)
        
        // when using iPad
        sheet.popoverPresentationController?.sourceView = sender as? UIView;
        // present the alert sheet
        self.presentViewController(sheet, animated: true, completion: nil)
        
    }

    @IBAction func graphPlotPressed(sender: AnyObject) {
        self.plotType = 1
        self.loadPhotoActions(sender)
        
    }
    @IBAction func rulerPlotPressed(sender: AnyObject) {
        
        self.plotType = 2
        self.loadPhotoActions(sender)
       
    }
    
    func imagePickerController(picker: UIImagePickerController, didFinishPickingImage image: UIImage, editingInfo: [String : AnyObject]?) {
        selectedImage = image
        hasSelectedImage = true
        picker.dismissViewControllerAnimated(true, completion: nil)
    }
    
    
    func imagePickerControllerDidCancel(picker: UIImagePickerController) {
        picker.dismissViewControllerAnimated(true, completion: nil)
    }
    
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        // code
        if (segue.identifier == "gotoEditView"){
            
            // fix the image orientation
            var fixedImage:UIImage = fixImageOrientation(selectedImage)
            
            // resize?
            let ratio = fixedImage.size.width / fixedImage.size.height
            let size  = CGSizeMake(1200 * ratio, 1200)
            fixedImage = resizeImage(fixedImage, targetSize: size)
            
            let EVC: EditViewController = segue.destinationViewController as! EditViewController
            EVC.selectedImage = fixedImage
            EVC.plotType = self.plotType
            
        } 
    }


}

