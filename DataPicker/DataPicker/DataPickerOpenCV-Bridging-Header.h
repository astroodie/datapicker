
//
//  DataPickerOpenCV-Bridging-Header.h
//  DataPicker
//
//  Created by Kuo-Chuan Pan on 7/3/15.
//  Copyright © 2015 Kuo-Chuan Pan. All rights reserved.
//


#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>

#ifndef DataPickerOpenCV_Bridging_Header_h
#define DataPickerOpenCV_Bridging_Header_h

#import "DropboxSDK.h"


@interface DataPickerOpenCV : NSObject {
    
}

+(UIImage *)cvDetectEdgeWithImage:(UIImage *)image;
+(UIImage *)cvGetGrayImage:(UIImage *)image;
+(UIImage *)cvGetPlotImage:(UIImage *)image;
+(UIImage *)cvTest:(UIImage *)image;
+(NSArray *)cvGetRect:(UIImage *)image;
+(UIImage *)cvGetTransferedImage:(UIImage *) image withRect:(NSArray *) inputs;

@end

#endif /* DataPickerOpenCV_Bridging_Header_h */