//
//  RangeView.swift
//  DataPicker
//
//       USE: for the detection of moving xmin, xmax, ymin, and ymax control points
//
//  Created by Kuo-Chuan Pan on 7/5/15.
//  Copyright © 2015 Kuo-Chuan Pan. All rights reserved.
//

import UIKit

protocol RangeViewDelegate: class{
    func cornerTapped(center:CGPoint, axis:Int, limit:Int)
    func cornerChanged(center:CGPoint, axis:Int, limit:Int)
    func cornerFinished(center:CGPoint, axis:Int, limit:Int)
}


class RangeView: UIView {
    var delegate:RangeViewDelegate?
    var lastLocation:CGPoint = CGPointMake(0, 0)
    var rangeColor = UIColor.greenColor()
    var axis: Int = 1  // 1 = x  ; 2 = y
    var limit: Int = 1 // 1 = min; 2 = max
    var xmax:CGFloat = 999
    var ymax:CGFloat = 999
    
    // Only override drawRect: if you perform custom drawing.
    // An empty implementation adversely affects performance during animation.
    /*
    override func drawRect(rect: CGRect) {
        // Drawing code
        
        
    } */

    
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        
        let panRecognizer = UIPanGestureRecognizer(target:self, action:"detectPan:")
        self.gestureRecognizers = [panRecognizer]
        
        self.layer.cornerRadius = 10
        self.backgroundColor = UIColor.greenColor()
        self.alpha = 0.5
        
    }
    
    required init(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }

    func detectPan(recognizer:UIPanGestureRecognizer) {
        
        let translation  = recognizer.translationInView(self.superview!)
        
        if (lastLocation.y + translation.y) < ymax && (lastLocation.x + translation.x) < xmax && (lastLocation.x + translation.x) > 10 && (lastLocation.y + translation.y) > 10 {
            self.center = CGPointMake(lastLocation.x + translation.x, lastLocation.y + translation.y)
        }
        self.delegate?.cornerChanged(self.center, axis: self.axis, limit:self.limit)
        self.setNeedsDisplay()
        
        
        if recognizer.state == UIGestureRecognizerState.Ended {
        
            self.delegate?.cornerFinished(self.center, axis: self.axis, limit:self.limit)
        }
    }
    
    override func touchesBegan(touches: Set<UITouch>, withEvent event: UIEvent?) {
        self.superview?.bringSubviewToFront(self)
        self.delegate?.cornerTapped(self.center, axis: self.axis, limit:self.limit)
        
        if self.center.y < ymax && self.center.x < xmax {
            lastLocation = self.center
        }
        
        
        //print("xmax \(xmax), ymax \(ymax)")
    }
    
    override func touchesEnded(touches: Set<UITouch>, withEvent event: UIEvent?) {
        self.delegate?.cornerFinished(self.center, axis: self.axis, limit:self.limit)
    }

    
}
