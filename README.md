# **DataPicker (alpha version)** #

A graph to data app for iOS. It allows you to generate plot data from a x-y plot.

**Note** that this app still have some issues. 
Currently, it only works for images in the landscape mode and output through email. 


## **Language** ##

* Swift 2.1

## **Requirements** ##

* iOS8.4 or later
* Xcode 7
* OpenCV2

## **License** ##

This software is created by K.-C. Pan and can be included as a part of any software-project free of charge. No credits are required, but aknowledgement in any form is appreciated.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.